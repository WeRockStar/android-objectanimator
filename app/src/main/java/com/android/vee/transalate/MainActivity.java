package com.android.vee.transalate;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(findViewById(R.id.figer) , "alpha", 1f, 0f, 1f);
        ObjectAnimator right = ObjectAnimator.ofFloat(findViewById(R.id.figer), "x" , 40f);
        objectAnimator.setRepeatCount(-1);
        right.setRepeatCount(-1);
        AnimatorSet aniSet = new AnimatorSet();
        aniSet.setDuration(500);
        aniSet.playTogether(objectAnimator, right);
        aniSet.start();
    }
}
